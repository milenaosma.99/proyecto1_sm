<?php 
session_start();
require_once "logica/Administrador.php";
require_once "logica/Producto.php";
require_once "logica/Cliente.php";
require_once "logica/Proveedor.php";
require_once "logica/Log.php";
 require_once "logica/Unidad.php";

//require_once "logica/Factura.php";

$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}else{
    $_SESSION["id"]="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>
<html>
<head>
	<link rel="icon" type="logo/jpg" href="img/carrito.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

   <script type="text/javascript"> 
        $(function () {
    	  $('[data-toggle="tooltip"]').tooltip()
    	})
    </script>
	
</head>
<body>
	<?php 
	$paginasSinSesion = array(
        "presentacion/autenticar.php",
	    "presentacion/cliente/registrarCliente.php",
	    	);	
	if(in_array($pid, $paginasSinSesion)){
	    include $pid;
	}else if($_SESSION["id"]!="") {
	    if($_SESSION["rol"] == "Administrador"){
	 
// 	        $administrador = new Administrador($_SESSION["id"]);
// 	        $administrador->consultar();
// 	         $accion="Ingreso al sistema";
// 	         $dato=$administrador->getNombre(). " " .$administrador->getApellido();
// 	         $actor=$_SESSION["rol"];
// 	        $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
// 	        $log->insertar();
	        
	        include "presentacion/menuAdministrador.php";
	    }else if($_SESSION["rol"] == "Cliente"){

	        
	        // 	        $cliente = new Cliente($_SESSION["id"]);
	        // 	        $cliente->consultar();
	        // 	         $accion="Ingreso al sistema";
	        // 	         $dato=$cliente->getNombre(). " " .$cliente->getApellido();
	        // 	         $actor=$_SESSION["rol"];
	        // 	        $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
	        // 	        $log->insertar();
	        
	        include "presentacion/menuCliente.php";
	    }else if($_SESSION["rol"] == "Proveedor"){
	    
	        
// 	        	        $proveedor = new Proveedor($_SESSION["id"]);
// 	        	        $proveedor->consultar();
// 	        	         $accion="Ingreso al sistema";
// 	        	         $dato=$proveedor->getNombre(). " " .$proveedor->getApellido();
// 	        	         $actor=$_SESSION["rol"];
// 	        	        $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
// 	        	        $log->insertar();
	        
	        include "presentacion/menuProveedor.php";
	    }
	    include $pid;
	}else{
	    include "presentacion/encabezado.php";
	    include "presentacion/inicio.php";
	}
	?>	
</body>
</html>
<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $idProveedor;
    private $foto;
    private $idUnidad;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = "",$idProveedor = "",$foto = "",$idUnidad= ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> idProveedor = $idProveedor;
        $this -> foto = $foto;
        $this -> idUnidad = $idUnidad;
    }

    public function consultar(){
        return "select nombre, cantidad, precio, idProveedor, foto, idUnidad
                from Producto
                where idProducto = '" . $this -> idProducto .  "'";
    }    
    
    function consultar2(){
        return "select idProducto, nombre, cantidad, precio, idProveedor, foto, idUnidad
                from Producto
                where idProveedor = '" . $this -> idProveedor . "'";
    }
    
    function buscar($filtro){
        return "select idProducto, nombre, cantidad, precio, idProveedor, foto, idUnidad
                from Producto
               where nombre like '" . $filtro . "%' ";
    }
    
    
    public function consultarTodos(){
        return "select idProducto, nombre, cantidad, precio, idProveedor, foto, idUnidad
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idProducto, nombre, cantidad, precio,  idProveedor, foto, idUnidad
                from Producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idProducto)
                from Producto";
    }
 
    public function editar(){
        return "update Producto
                set nombre = '" . $this -> nombre . "', cantidad = '" . $this -> cantidad . "', precio = '" . $this -> precio . "'," . (($this -> foto!="")?", foto = '" . $this -> foto . "'":""). "', idUnidad = '" . $this -> idUnidad  . "
                where idProducto = '" . $this -> idProducto .  "'";
    }
    
    
    public function insertar(){
        return "insert into Producto (nombre, cantidad, precio, idProveedor, foto, idUnidad)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "' , '" . $this -> precio . "', '" . $this -> idProveedor . "', '". (($this -> foto!="")?"'" . $this -> foto . "'":"") . "', '" . $this -> idUnidad . "')";
    }
}

?>
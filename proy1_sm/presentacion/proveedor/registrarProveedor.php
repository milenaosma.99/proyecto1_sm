<?php 
$error = 0;
$registrado = false;
if(isset($_POST["registrar"])){
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
    $proveedor = new Proveedor("", $nombre, $apellido, $correo, $clave);
    if($proveedor-> existeCorreo()){
        $error = 1;
    }else{
        $proveedor -> registrar();
        $registrado = true;
    }
    
    $proveedor->consultar();
    
//     $accion="Registro proveedor" ." ". $nombre ." ". $apellido ;
//     $dato=$administrador->getNombre(). " " .$administrador->getApellido();
//     $actor=$_SESSION["rol"];
//     $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//     $log->insertar();
    
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro Proveedor</h4>
				</div>
              	<div class="card-body">
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Registro exitoso
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	<form action="index.php?pid=<?php echo base64_encode("presentacion/proveedor/registrarProveedor.php") ?>" method="post">
        				
        				<div class="form-group">
    						<input name="nombre" type="text" class="form-control" placeholder="Nombre" required>
    					</div>
        				<div class="form-group">
    						<input name="apellido" type="text" class="form-control" placeholder="Apellido" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        				<div class="form-group">
    					<center><button type="submit" name="registrar" class="btn btn-dark">Agregar</button> <br></center>
        			    </div>
    					</form>     
        		</div>
            </div>		
		</div>
	</div>
</div>


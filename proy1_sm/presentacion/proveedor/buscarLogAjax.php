<?php
$filtro = $_GET["filtro"];
$log = new Log();
$logs = $log -> consultarFiltro2($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Log</h4>
				</div>
				<div class="text-right"><?php echo count($logs) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Actor</th>
							
						</tr>
						<?php 
						$i=1;
						foreach($logs as $logActual){
// 						    $posiciones = array();
// 						    for($i=0; $i<strlen($productoActual -> getNombre())-strlen($filtro)+1; $i++){
// 						        if(strtolower(substr($productoActual -> getNombre(), $i, strlen($filtro))) == strtolower($filtro)){
// 						            array_push($posiciones, $i);
// 						        }
// 						    }
						    $pos = stripos($logActual -> getAccion(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $logActual -> getAccion() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($logActual -> getAccion(), 0, $pos) . "<mark>" . substr($logActual -> getAccion(), $pos, strlen($filtro)) . "</mark>" . substr($logActual -> getAccion(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $logActual -> getDatos() . "</td>";
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "<td>" . $logActual -> getActor() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
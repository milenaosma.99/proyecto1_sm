<?php
$proveedor = new Proveedor($_SESSION['id']);
$proveedor->consultar();

if (isset($_POST["actualizar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $proveedor = new Proveedor($_SESSION["id"], $nombre, $apellido);
    $proveedor->actualizarDatos();
    
//     $accion="Actualizar datos";
//     $dato=$proveedor->getNombre(). " " .$proveedor->getApellido();
//     $actor=$_SESSION["rol"];
//     $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//     $log->insertar();
    
}
?>
<br></br>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-dark text-white">Editar Datos</div>
				<div class="card-body">
						<?php
    if (isset($_POST["actualizar"])) {
        ?>
						<div class="alert alert-success" role="alert">Datos actualizados
						exitosamente.</div>						
						<?php } ?>
						<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/proveedor/actualizarDatos.php")."&idProveedor=".$_SESSION["id"] ?>
						method="post">
						<div class="form-group">
							<input type="text" name="nombre" class="form-control"
								placeholder="Nombre" required="required"
								value="<?php echo $proveedor->getNombre(); ?>">
						</div>
						<div class="form-group">
							<input type="text" name="apellido" class="form-control"
								placeholder="apellido" required="required"
								value="<?php echo $proveedor->getApellido(); ?>">
						</div>
						<button type="submit" name="actualizar" class="btn btn-dark">Actualizar</button>
    			</div>
			</div>
		</div>

	</div>

</div>
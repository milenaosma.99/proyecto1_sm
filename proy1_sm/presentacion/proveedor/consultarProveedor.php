
<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

$proveedor = new Proveedor();
$proveedores = $proveedor->consultarTodos();

// $accion="Consultar Proveedores";
// $dato=$administrador->getNombre(). " " .$administrador->getApellido();
// $actor=$_SESSION["rol"];
// $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
// $log->insertar();

?>

		<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Proveedor</h4>
				</div>
				<div class="text-right"><?php echo count($proveedores) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>cambiar estado</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($proveedores as $p){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getNombre() . "</td>";
						    echo "<td>" . $p -> getApellido() . "</td>";
						    echo "<td>" . $p -> getCorreo() . "</td>";
						    echo "<td><div id='estado".$p->getIdProveedor()."'>".($p -> getEstado()==1?"Habilitado":"Inhabilitado")."</div></td>";                 
						    echo "<td>";
						    echo "<a id='cambiarEstado".$p->getIdProveedor()."' href='#'><div id='iconoEstado".$p->getIdProveedor()."'><i id='icono".$p->getIdProveedor()."' class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title=". (($p->getIdProveedor()==1?"Habilitado":"Deshabilitado")) ."></i></div></a>";
						    echo "<a class='fas fa-cog' href='index.php?pid=" . base64_encode("presentacion/producto/consultarProductos.php") . "&idProveedor=" . $p->getIdProveedor() . "' data-toggle='tooltip' data-placement='left' title='Consultar Productos'> </a>";
						    echo "</td>";
						    echo "</tr>";
						     $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
<?php 
foreach ($proveedores as $e) {
    echo "$(\"#cambiarEstado".$e->getIdProveedor()."\").click(function(){\n";
    echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/proveedor/cambiarEstadoProveedorAjax.php") . "&idProveedor=".$e->getIdProveedor()."\";\n";
    echo "$(\"#estado".$e->getIdProveedor()."\").load(ruta);\n";
    echo "$(\"#icono".$e->getIdProveedor()."\").tooltip('hide');\n";
    echo "ruta1 = \"indexAjax.php?pid=" . base64_encode("presentacion/proveedor/cambiarIconoEstadoProveedorAjax.php") . "&idProveedor=".$e->getIdProveedor()."\";\n";
    echo "$(\"#iconoEstado".$e->getIdProveedor()."\").load(ruta1);\n";
    echo "});\n";
    
    
}
?>
});
</script>
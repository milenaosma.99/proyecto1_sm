<?php
$proveedor = new Proveedor($_SESSION['id']);
$proveedor ->consultar();

if (isset($_POST["actualizar"])) {
    $clave = $_POST["clave"];
    $proveedor = new Proveedor($_SESSION["id"]," "," ", " ",$clave);
    $proveedor->actualizarClave();
}
?>
<br></br>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-dark text-white">Editar Contrase&ntilde;a</div>
				<div class="card-body">
						<?php
    if (isset($_POST["actualizar"])) {
        ?>
						<div class="alert alert-success" role="alert">Contrase&ntilde;a actualizada
						exitosamente.</div>						
						<?php } ?>
						<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/proveedor/actualizarContraseña.php")."&idCliente=".$_SESSION["id"] ?>
						method="post">
						<div class="input-group">
							<input id="txtPassword"  type="Password" name="clave" class="form-control"
								placeholder="Nueva contrase&ntilde;a" required="required"
								value="<?php echo $proveedor->getClave(); ?>">
								<div class="input-group-append">
            					<button id="show_password" class="btn btn-dark" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
          							</div>
									</div><br>
						<button type="submit" name="actualizar" btn-center class="btn btn-dark">Actualizar</button>
    			</div>
			</div>
		</div>

	</div>

</div>

<script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("txtPassword");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
});
</script>
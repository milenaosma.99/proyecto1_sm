<?php 
$filtro = $_GET["filtro"];
$proveedor = new Proveedor($_GET["idProveedor"]);
$proveedor ->consultar();
$logs = $log -> consultarFiltro3($filtro);

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Log proveedor: <?php echo $proveedor->getNombre() ." ".$proveedor->getApellido() ?></h4>
				</div>
				<div class="text-right"><?php echo count($logs) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Fecha</th>
							<th>Hora</th>
							
						</tr>
						<?php 
						$i=1;
						foreach($logs as $logActual){
// 						    $posiciones = array();
// 						    for($i=0; $i<strlen($productoActual -> getNombre())-strlen($filtro)+1; $i++){
// 						        if(strtolower(substr($productoActual -> getNombre(), $i, strlen($filtro))) == strtolower($filtro)){
// 						            array_push($posiciones, $i);
// 						        }
// 						    }
						    $pos = stripos($logActual -> getAccion(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $logActual -> getAccion() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($logActual -> getAccion(), 0, $pos) . "<mark>" . substr($logActual -> getAccion(), $pos, strlen($filtro)) . "</mark>" . substr($logActual -> getAccion(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>

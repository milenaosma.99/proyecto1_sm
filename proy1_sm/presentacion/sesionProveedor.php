<?php
$proveedor = new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-success">
					<h4>Bienvenido proveedor</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-3">
              				<img src="img/user.png" width="100%" ">              			
              			</div>
              			<div class="col-9">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $proveedor-> getNombre() ?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $proveedor -> getApellido() ?></td>
								</tr> 
								<tr>
									<th>Correo</th>
									<td><?php echo $proveedor -> getCorreo() ?></td>
								</tr>
							</table>
						</div>              		
              		</div>              	
            	</div>
            </div>
		</div>
	</div>
</div>

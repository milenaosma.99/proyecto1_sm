<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();

?>

<nav class="navbar navbar-expand-md navbar-light bg-primary">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPagina.php") ?>">Consultar todos</a>
				</div></li>		
		
					<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Proveedor</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/registrarProveedor.php") ?>">Agregar prov</a>
				<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/consultarProveedor.php") ?>">consultar proveedores</a>
		
					<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Clientes</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarCliente.php") ?>">consultar clientes</a>
		
					<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Log</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/buscarLog.php") ?>">Log Cliente</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/buscarLog.php") ?>">Log Proveedor</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/buscarLogAdministrador.php") ?>">Log Admin</a>
	
				</div></li>
		</ul>
		
		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() ?> <?php echo $administrador -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/administrador/actualizarDatos.php") ?>">Editar Perfil</a> 
						<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/actualizarContraseña.php") ?>">Cambiar Clave</a>
				</div></li>
			<li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>
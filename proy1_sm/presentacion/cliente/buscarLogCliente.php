<?php 
$cliente = new Cliente($_SESSION["id"]);
$cliente ->consultar();

//     $accion="Ver log";
//     $dato=$cliente->getNombre(). " " .$cliente->getApellido();
//     $actor=$_SESSION["rol"];
//     $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//     $log->insertar();

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Buscar Log cliente: <?php echo $cliente->getNombre() ." ".$cliente->getApellido() ?></h4>
				</div>
				<div class="card-body">
					<input type="text" id="filtro" class="form-control"
						placeholder="Palabra clave">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="resultados"></div>
<script>
$(document).ready(function(){
    $("#filtro").keyup(function() {
        if($(this).val().length >= 3){            
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/buscarLogClienteAjax.php") ?>&idCliente=<?php echo $_SESSION["id"] ?>&filtro=" + $(this).val();
    		$("#resultados").load(url);
        }
    });
});
</script>



<?php
$cliente = new Cliente($_SESSION['id']);
$cliente->consultar();

// $accion="Actualizar datos";
// $dato=$cliente->getNombre(). " " .$cliente->getApellido();
// $actor=$_SESSION["rol"];
// $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
// $log->insertar();


if (isset($_POST["actualizar"])) {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $cliente = new Cliente($_SESSION["id"], $nombre, $apellido);
    $cliente->actualizarDatos();
    
    $accion="Actualizar datos";
    $dato=$cliente->getNombre(). " " .$cliente->getApellido();
    $actor=$_SESSION["rol"];
    $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
    $log->insertar();
    
}
?>
<br></br>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-dark text-white">Editar Datos</div>
				<div class="card-body">
						<?php
    if (isset($_POST["actualizar"])) {
        ?>
						<div class="alert alert-success" role="alert">Datos actualizados
						exitosamente.</div>						
						<?php } ?>
						<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/cliente/actualizarDatos.php")."&idCliente=".$_SESSION["id"] ?>
						method="post">
						<div class="form-group">
							<input type="text" name="nombre" class="form-control"
								placeholder="Nombre" required="required"
								value="<?php echo $cliente->getNombre(); ?>">
						</div>
						<div class="form-group">
							<input type="text" name="apellido" class="form-control"
								placeholder="apellido" required="required"
								value="<?php echo $cliente->getApellido(); ?>">
						</div>
						<button type="submit" name="actualizar" class="btn btn-dark">Actualizar</button>
    			</div>
			</div>
		</div>

	</div>

</div>s
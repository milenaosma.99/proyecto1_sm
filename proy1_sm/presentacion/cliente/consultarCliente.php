<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

// $accion="Consultar clientes";
// $dato=$administrador->getNombre(). " " .$administrador->getApellido();
// $actor=$_SESSION["rol"];
// $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
// $log->insertar();

$cliente = new Cliente();
$clientes = $cliente->consultarTodos();


?>
		<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Cliente</h4>
				</div>
				<div class="text-right"><?php echo count($clientes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Estado</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($clientes as $c){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $c -> getNombre() . "</td>";
						    echo "<td>" . $c -> getApellido() . "</td>";
						    echo "<td>" . $c -> getCorreo() . "</td>";
						    echo "<td><div id='estado".$c->getIdCliente()."'>".($c -> getEstado()==1?"Habilitado":"Inhabilitado")."</div></td>";
						    echo "<td>";
						    echo "<a id='cambiarEstado".$c->getIdCliente()."' href='#'><div id='iconoEstado".$c->getIdCliente()."'><i id='icono".$c->getIdCliente()."' class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title=". (($c->getIdCliente()==1?"Habilitado":"Deshabilitado")) ."></i></div></a>";
						    echo "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
<?php 
foreach ($clientes as $c) {
    echo "$(\"#cambiarEstado".$c->getIdCliente()."\").click(function(){\n";
    echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/cambiarEstadoClienteAjax.php") . "&idCliente=".$c->getIdCliente()."\";\n";
    echo "$(\"#estado".$c->getIdCliente()."\").load(ruta);\n";
    echo "$(\"#icono".$c->getIdCliente()."\").tooltip('hide');\n";
    echo "ruta1 = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/cambiarIconoEstadoClienteAjax.php") . "&idCliente=".$c->getIdCliente()."\";\n";
    echo "$(\"#iconoEstado".$c->getIdCliente()."\").load(ruta1);\n";
    echo "});\n";
}
?>
});
</script>
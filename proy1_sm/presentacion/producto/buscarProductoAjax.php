<?php
$filtro = $_GET['filtro'];
$producto = new Producto();
$productos = $producto -> buscar($filtro);

if(count($productos)>0){
    ?>

					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad disponible</th>
							<th>Unidad</th>
							<th>Precio</th>
							<th>Foto</th>
							<th>Servicios</th>
						<th></th>
						</tr>
						<?php 
						
						$i=1;
						foreach($productos as $productoActual){
						    
						    $unidad = new Unidad($productoActual->getIdUnidad());
						    $unidad->consultar();
						    						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getNombre() . "</td>";
						    echo "<td>" . $productoActual -> getCantidad() . "</td>";
						    echo "<td>" . $unidad->getNombre() . "</td>";
						    echo "<td>" . $productoActual -> getPrecio() . "</td>";
						    echo "<td>" . (($productoActual -> getFoto()!="")?"<img src='" . $productoActual -> getFoto() . "' height='80px'>":"") . "</td>";
						    echo "<td><div class='form-group'>";
						   ?>
						    <form action="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?> " method='post'" >
						   <?php 
						    echo "<input type='number' name='cantidad".$productoActual->getIdProducto()."' class='form-control' placeholder='Cantidad' min='1'  required></div>";
						    echo "<a id='agregar".$productoActual->getIdProducto()."' href='#'><div id='iconoEstado".$productoActual->getIdProducto()."'><i id='icono".$productoActual->getIdProducto()."' class='fas fa-cart-arrow-down' data-toggle='tooltip' data-placement='left' title='Aniadir'></i></div></a></form>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div id="lista"></div>
				
<?php } else { ?>
<div class="alert alert-danger alert-dismissible fade show"
	role="alert">
	No se encontraron resultados
	<button type="button" class="close" data-dismiss="alert"
		aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<?php } ?>
<script>
$(document).ready(function(){
<?php 
foreach ($productos as $p) {
    echo "$(\"#agregar".$p->getIdProducto()."\").click(function(){\n";
    $cantidad=$_POST["cantidad".$productoActual->getIdProducto()];
    echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/producto/carritoAjax.php") . "&idProducto=".$p->getIdProducto()."&cantidad=".$cantidad."\";\n";
    echo "$(\"#lista".$p->getIdProducto()."\").load(ruta);\n";
    echo "});\n";
}
?>
});
</script>
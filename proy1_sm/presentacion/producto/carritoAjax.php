<?php 
$producto=new Producto($_GET["idProducto"]);
$producto->consultar();
$cantidad=$_GET["cantidad".$_GET["idProducto"]];
?>
<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad Solicitada</th>
							<th>Unidad</th>
							<th>SubTotal</th>
							<th>Foto</th>
						<th></th>
						</tr>
						<?php 
						
						$i=1;
						foreach($producto as $productoActual){
						    
						    $unidad = new Unidad($productoActual->getIdUnidad());
						    $unidad->consultar();
						    $precio=$productoActual -> getPrecio();
						    						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getNombre() . "</td>";
						    echo "<td>" . $cantidad . "</td>";
						    echo "<td>" . $unidad->getNombre() . "</td>";
						    echo "<td>" . $precio*$cantidad . "</td>";
						    echo "<td>" . $productoActual -> getFoto() . "</td>";
						    echo "<td><div class='form-group'>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
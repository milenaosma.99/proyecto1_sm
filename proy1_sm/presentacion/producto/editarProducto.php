<?php
if(isset($_POST["editar"])){
    if($_FILES["foto"]["name"] != ""){
        $rutaLocal = $_FILES["foto"]["tmp_name"];
        $tipo = $_FILES["foto"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $producto = new Producto($_GET["idProducto"]);
        $producto -> consultar();
        if($producto -> getFoto() != ""){
            unlink($producto -> getFoto());
        }
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"], $rutaRemota, $_POST["idUnidad"]);
        $producto -> editar();
    }else{
        $producto = new Producto($_GET["idProducto"], $_POST["nombre"], $_POST["cantidad"], $_POST["precio"], $_POST["idUnidad"]);
        $producto -> editar();
    }
}else{
    $producto = new Producto($_GET["idProducto"]);
    $producto -> consultar();

//     $accion="Editar Producto";
//     $dato=$proveedor->getNombre()." ".$proveedor->getApellido();
//     $actor=$_SESSION["rol"];
//     $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//     $log->insertar();
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/editarProducto.php") ?>&idProducto=<?php echo $_GET["idProducto"]?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $producto -> getNombre() ?>" required>
						</div>
						<div class="form-group">
							<label>Unidad</label>
							<select class="form-control"
								name="idUnidad">
								
								<?php
								$unidad = new Unidad();
								$unidades = $unidad -> consultarTodos();
								foreach ($unidades as $c){
								    echo "<option value='" . $c -> getIdUnidad() . "'>" . $c -> getNombre() .  "</option>";
								}
								?>
							</select>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $producto -> getCantidad() ?>" required>
						</div>
							
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $producto -> getPrecio() ?>" required>
						</div>
						
						<div class="form-group">
							<label>Foto</label> 
							<input type="file" name="foto" class="form-control" >
						</div>
						
						<button type="submit" name="editar" class="btn btn-dark">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
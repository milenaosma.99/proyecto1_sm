<?php
$proveedor = new Proveedor($_GET["idProveedor"]);
$proveedor->consultar();
$producto = new Producto("","","","",$_GET["idProveedor"]);
$productos = $producto->consultar2();

//         $accion="Consultar productos ". $proveedor->getNombre(). " " .$proveedor->getApellido() ;
//         $administrador= new Administrador($_SESSION["id"]);
//         $administrador->consultar();
//         $dato=$administrador->getNombre(). " " .$administrador->getApellido();
//         $actor=$_SESSION["rol"];
//         $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//         $log->insertar();


?>


		<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar productos: <?php echo  " " .$proveedor->getNombre(). " " .$proveedor->getApellido()?> </h4>
				</div>
				<div class="text-right"><?php echo count($productos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Foto</th>
							<th>Unidad</th>
							
						</tr>
						<?php 
						$i=1;
						foreach($productos as $p){
						    
						    $unidad = new Unidad($p->getIdUnidad());
						    $unidad->consultar();
						    
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $p -> getNombre() . "</td>";
						    echo "<td>" . $p -> getCantidad() . "</td>";
						    echo "<td>" . $p -> getPrecio() . "</td>";
						    echo "<td>" . (($p-> getFoto()!="")?"<img src='" . $p-> getFoto() . "' height='80px'>":"") . "</td>";
						    echo "<td>" . $unidad -> getNombre() . "</td>";
						    echo "</tr>";
						     $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>




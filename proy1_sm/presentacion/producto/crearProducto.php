<?php

if(isset($_POST["crear"])){
    
    if($_FILES["foto"]["name"] != ""){
        $rutaLocal = $_FILES["foto"]["tmp_name"];
        $tipo = $_FILES["foto"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "imagenes/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $producto = new Producto("", $_POST["nombre"], $_POST["cantidad"], $_POST["precio"],$_SESSION["id"], $rutaRemota, $_POST["idUnidad"]);
        $producto -> insertar();
        
//         $accion="Agregar producto";
//         $dato=$proveedor->getNombre(). " " .$proveedor->getApellido();
//         $actor=$_SESSION["rol"];
//         $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//         $log->insertar();
 
    }else{
        
        $producto = new Producto("", $_POST["nombre"], $_POST["cantidad"], $_POST["precio"],$_SESSION["id"],"", $_POST["idUnidad"]);
        $producto -> insertar();
        
    }
    
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-success">
					<h4>Agregar Producto</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Unidad</label>
							<select class="form-control"
								name="idUnidad">
								
								<?php
								$unidad = new Unidad();
								$unidades = $unidad -> consultarTodos();
								foreach ($unidades as $c){
								    echo "<option value='" . $c -> getIdUnidad() . "'>" . $c -> getNombre() .  "</option>";
								}
								?>
							</select>
						</div>

						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1"  required>
						</div>
					
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1"  required>
						</div>
								
								
						<div class="form-group">
							<label>Foto</label> 
							<input type="file" name="foto" class="form-control" >
						</div>
								
						<button type="submit" name="crear" class="btn btn-success">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
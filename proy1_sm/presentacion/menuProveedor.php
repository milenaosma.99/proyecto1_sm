<?php
$proveedor = new Proveedor($_SESSION["id"]);
$proveedor -> consultar();
?>
<nav class="navbar navbar-expand-md navbar-light bg-success">
	<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionProveedor.php") ?>"><i class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false">Producto</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">Agregar</a>
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductosProv.php") ?>">Consultar productos</a>
				</div></li>
						<li class="nav-item active"><a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/buscarLogProveedor.php") ?>">Log</a></li>
			
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
				href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
				aria-haspopup="true" aria-expanded="false"><span class="badge badge-danger"></span>
				Proveedor: <?php echo ($proveedor -> getNombre()!=""?$proveedor -> getNombre():$proveedor-> getCorreo()) ?> <?php echo $proveedor -> getApellido() ?></a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/actualizarDatos.php") ?>">Editar Datos</a> 
					<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/actualizarContraseña.php") ?>">Cambiar Clave</a>						
				</div></li>
			<li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
		</ul>
	</div>
</nav>
<?php

$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$proveedor = new Proveedor("", "", "", $correo, $clave);
if($administrador -> autenticar()){
    $_SESSION["id"] = $administrador -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    
    $administrador= new Administrador($_SESSION["id"]);
    $administrador->consultar();
    $accion="Ingreso al sistema";
    $dato=$administrador->getNombre(). " " .$administrador->getApellido();
    $actor=$_SESSION["rol"];
//     $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//     $log->insertar();
    
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else if($cliente -> autenticar()){
    if($cliente -> getEstado() == -1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
       header("Location: index.php?error=3");        
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";

        $cliente = new Cliente($_SESSION["id"]);
        $cliente->consultar();
        $accion="Ingreso al sistema";
        $dato=$cliente->getNombre(). " " .$cliente->getApellido();
        $actor=$_SESSION["rol"];
//         $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//         $log->insertar();
        
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
    }
}else if($proveedor -> autenticar()){
        if($proveedor -> getEstado() == -1){
            header("Location: index.php?error=2");
        }else if($proveedor -> getEstado() == 0){
            header("Location: index.php?error=3");
        }else{
            $_SESSION["id"] = $proveedor -> getIdProveedor();
            $_SESSION["rol"] = "Proveedor";
        
            $proveedor= new Proveedor($_SESSION["id"]);
            $proveedor->consultar();
            $accion="Ingreso al sistema";
            $dato=$proveedor->getNombre(). " " .$proveedor->getApellido();
            $actor=$_SESSION["rol"];
//             $log = new Log("", $accion, $dato, date('Y-m-d'), date('H:i:s'),$actor );
//             $log->insertar();
            header("Location: index.php?pid=" . base64_encode("presentacion/sesionProveedor.php"));
        }
    }else{
        header("Location: index.php?error=1");
    }
    
    ?>
<?php

$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

if (isset($_POST["actualizar"])) {
$log = new Log($_POST["idLog"]);
$log->crear();
    $clave = $_POST["clave"];
    $administrador = new Administrador($_SESSION["id"]," "," ", " ",$clave);
    $administrador->actualizarClave();
}
?>
<br></br>
<div class="container">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header bg-dark text-white">Editar Contrase&ntilde;a</div>
				<div class="card-body">
						<?php
    if (isset($_POST["actualizar"])) {
        ?>
						<div class="alert alert-success" role="alert">Contrase&ntilde;a actualizada
						exitosamente.</div>						
						<?php } ?>
						<form
						action=<?php echo "index.php?pid=" . base64_encode("presentacion/administrador/actualizarContraseña.php")."&idAdministrador=".$_SESSION["id"] ?>
						method="post">
						<div class="input-group">
							<input id="txtPassword"  type="Password" name="clave" class="form-control"
								placeholder="Nueva contrase&ntilde;a" required="required"
								value="<?php echo $administrador->getClave(); ?>">
								<div class="input-group-append">
            					<button id="show_password" class="btn btn-dark" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
          							</div>
									</div><br>
						<button type="submit" name="actualizar" btn-center class="btn btn-dark">Actualizar</button>
    			</div>
			</div>
		</div>

	</div>

</div>

<script type="text/javascript">
function mostrarPassword(){
		var cambio = document.getElementById("txtPassword");
		if(cambio.type == "password"){
			cambio.type = "text";
			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
		}else{
			cambio.type = "password";
			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
		}
	} 
	
	$(document).ready(function () {
	//CheckBox mostrar contraseña
	$('#ShowPassword').click(function () {
		$('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});
});
</script>
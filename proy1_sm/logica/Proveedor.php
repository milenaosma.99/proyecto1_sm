<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProveedorDAO.php";
class Proveedor{
    private $idProveedor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;      
    private $conexion;
    private $proveedorDAO;

    public function getIdProveedor(){
        return $this -> idProveedor;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }
    
    public function getEstado(){
        return $this -> estado;
    }
    
    public function Proveedor($idProveedor = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $estado = ""){
        $this -> idProveedor = $idProveedor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> proveedorDAO = new ProveedorDAO($this -> idProveedor, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> estado);
    }
   
    public function existeCorreo(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> proveedorDAO -> existeCorreo());        
        $this -> conexion -> cerrar();        
        return $this -> conexion -> numFilas();
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idProveedor = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }

    }
       public function cambiarEstado() {
            $this -> conexion -> abrir();
            $this->conexion->ejecutar($this->proveedorDAO -> cambiarEstado());
            $this -> conexion -> cerrar();
        }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarTodos());
        $proveedores= array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Proveedor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"",$resultado[4]);
            array_push($proveedores, $p);
        }
        $this -> conexion -> cerrar();
        return $proveedores;
    }
    
    public function consultar(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO-> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idProveedor = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> apellido = $resultado[2];
        $this -> correo =  $resultado[3];
        $this -> estado =  $resultado[4];
    }

    public function actualizarDatos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO->actualizarDatos());
        $this -> conexion -> cerrar();
    }
    
    
    public function actualizarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO ->actualizarClave());
        $this -> conexion -> cerrar();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
}

?>
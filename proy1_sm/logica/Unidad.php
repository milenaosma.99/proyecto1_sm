<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/UnidadDAO.php";
class Unidad{
    private $idUnidad;
    private $nombre;
    private $conexion;
    private $unidadDAO;

  
    public function getIdUnidad()
    {
        return $this->idUnidad;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function Unidad($idUnidad = "", $nombre= ""){
        $this -> idUnidad= $idUnidad;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> unidadDAO = new UnidadDAO($idUnidad, $nombre);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> unidadDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idUnidad = $resultado[0];
        $this -> nombre = $resultado[1];
    }

       
       function consultarTodos(){
           $this -> conexion -> abrir();
           $this -> conexion -> ejecutar($this -> unidadDAO-> consultarTodos());
           $registros = array();
           for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
               $registro = $this -> conexion -> extraer();
               $registros[$i] = new Unidad($registro[0], $registro[1]);
           }
           return $registros;
       }
       
       
}


?>
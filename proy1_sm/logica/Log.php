<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogDAO.php";
class Log{
    private $idLog;
    private $accion;
    private $dato;
    private $fecha;
    private $hora;
    private $actor;
    private $conexion;
    private $administradorDAO;

  
    public function getIdLog()
    {
        return $this->idLog;
    }

    public function getAccion()
    {
        return $this->accion;
    }

    public function getDatos()
    {
        return $this->datos;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function getActor()
    {
        return $this->actor;
    }


    public function Log($idLog = "", $accion= "", $datos = "", $fecha= "", $hora = "", $actor = ""){
        $this -> idLog= $idLog;
        $this -> accion = $accion;
        $this -> datos = $datos;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> actor = $actor;
        $this -> conexion = new Conexion();
        $this -> logDAO = new LogDAO($this -> idLog, $this -> accion, $this -> datos, $this -> fecha, $this -> hora, $this -> actor);
    }

    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    //desarrollar esto
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
    }

    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltro($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $l);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarFiltro1($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltro1($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $l);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    public function consultarFiltro2($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltro2($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($logs, $l);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
    public function consultarFiltro3($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltro3($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1],"", $resultado[2], $resultado[3]);
            array_push($logs, $l);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
}

?>